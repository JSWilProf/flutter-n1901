import 'dart:math';

void main() {
  var rand = Random();

  List<int> lista = List.generate(10, (_) => rand.nextInt(11));

  lista
      .where((x) => x >= 5)
      .forEach((n) => print('$n é ${ehPar(n) ? 'par' : 'impar'}'));

  for (var n in lista) {
    if (n >= 5) print('$n é ${ehPar(n) ? 'par' : 'impar'}');
  }
}

bool ehPar(int n) => n % 2 == 0;