void main() {
  List<int> lista = [1, 2, 3, 3, 4];
  
  for(int i = 0;i < lista.length;i++) {
    int numero = lista[i];
    print('${i+1} : $numero');
  }
  
  int pos = 1;
  for(int numero in lista) {
    print('${pos++} : $numero');
  }
  
  pos = 1;
  lista.forEach((numero) {
    print('${pos++} : $numero');
  });
}