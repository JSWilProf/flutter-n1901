import 'dart:math';

void main() {
  var rand = Random();

  List<int> lista = List.generate(10, (_) => rand.nextInt(11));

  print(lista);
  print(maiorQueAMedia(lista));
}

List<int> maiorQueAMedia(List<int> lista) {
  // Calcular a média
  var media = lista.reduce((total, numero) => total + numero) / lista.length;
  // Separar os valores da lista que são
  //    maiores que méia calculada
  return lista.where((numero) => numero > media).toList();
}
