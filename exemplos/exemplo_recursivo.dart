import 'dart:math';

void main() {
  var rand = Random();
  
  List<int> lista = List.generate(10, (_) => rand.nextInt(11));
  
  
  lista.forEach((numero) => print('teste($numero) = ${teste(numero)}'));
  
//   for(var numero in lista)
//     print('teste($numero) = ${teste(numero)}');
}

// f(n) -> f(n - 1) * 2 para n > 2
int teste(int n) {
  if(n > 2) return teste(n - 1) * 2;
  return 1;
  
  //return n > 2 ? teste(n - 1) * 2 : 1;
}