import 'dart:math';

void main() {
  var rand = Random();
  
  List<int> lista = List.generate(10, (_) => rand.nextInt(11));
  
  lista.forEach((numero) => print('fat($numero) = ${fat(numero)}'));
}

int fat(int n) => (n > 1) ? fat(n - 1) * n : 1;