import 'dart:math';

void main() {
  var rand = Random();
  
  List<int> lista = List.generate(10, (_) => 
                rand.nextInt(11));
  
  lista.forEach((n) => 
     print('$n é ${ehPar(n) ? 'par' : 'impar'}'));
  
  for(var n in lista) 
    print('$n é ${ehPar(n) ? 'par' : 'impar'}');
  
}

bool ehPar(int numero) => numero % 2 == 0;