import 'dart:math';

void main() {
  var random = Random();
  
  var lista = List.generate(10, (_) => random.nextInt(11));
  
  print(lista);
  
  var resposta = maiorQueAMedia(lista).entries.toList();
  
  print('\nMédia: ${resposta[0].key}\n' +
        'nºs maiores que a média:\n${resposta[0].value}');
}

Map<double, String> maiorQueAMedia(List<int> lista) {
  var media = lista.reduce((a, b) => a + b) / lista.length;
  return { media : lista.where((n) => n > media)
    .map((n) => '$n')
    .reduce((a, b) => '$a, $b') };  
}