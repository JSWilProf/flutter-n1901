class Pessoa {
  final _nome;
  var  email;
 
  Pessoa(this._nome, {this.email});

  String comprimento(String quem) => 'Olá, $quem. Eu sou $_nome';
  
  @override
  String toString() => 'Nome: $_nome, E-Mail: $email';
}

class Impostor extends Pessoa {
  get _nome => '';
  
  Impostor(String nome) : super(nome);
  
  @override
  String comprimento(String quem) => 'Olá, $quem. Você sabe quem sou?';
  
  @override
  String toString() => 'E-Mail: $email';
}


main() {
  var p = Pessoa("João", email: "joao@gmail.com");
  var i = Impostor("Joaquim");
  i.email = "joca@yahoo.com.br";
  
  print('[$p]');
  print('[$i]');
}