import 'package:intl/intl.dart';

main() {
  final formataData = DateFormat("dd 'de' MMMM 'de' yyyy");

  var data = DateTime(2019, 1, 30);
  print('Esta data é valida: ${formataData.format(data)}');

  final formataNumeros = NumberFormat('##0.00', 'pt_BR');

  String dividendos = formataNumeros.format(2543365.44);
  double valor = formataNumeros.parse(dividendos) * 0.9;
  print('$dividendos : $valor');
}
