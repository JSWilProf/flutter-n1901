class Pessoa {
  final _nome;
 
  Pessoa(this._nome);

  String comprimento(String quem) => 'Olá, $quem. Eu sou $_nome';
}

mixin Colega {
  String nome;
  String comprimento(String quem) => 'Olá, $quem. Eu sou $nome';
}

class Impostor with Colega {
  Impostor(nome) {
    this.nome = nome;
  }
  
  @override
  String comprimento(String quem) => 'Olá, $quem. Eu sou $nome';
}


main() {
  var i = Impostor("Ziza");
  
  print(i.comprimento("Zeca"));
}